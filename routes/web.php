<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('homenew');
Route::get('/customerlogin', 'RoleController@userRoleView');

Route::get('/admin-dashboard', function(){
    return View('admin.admin');
});
Route::get('/create-product','ProductController@product');
Route::post('/insert-product','ProductController@productInsert');
Route::get('/view-product','ProductController@allProduct');
Route::get('/product-edit/{id}','ProductController@productEdit');
Route::patch('/product-updated/{id}','ProductController@productupdated');
Route::get('/delete-product/{id}','ProductController@productDelete');
Route::delete('/delete_product_data/{id}','ProductController@deletedproduct');

Route::get('/customer-dashboard', function(){
    return View('customer.customer');
});
Route::get('/show-product/','ProductController@purchaseProduct');

Route::post('/cart-product/{id}','ProductController@cartProduct');

Route::get('/view-cart','ProductController@viewCartProduct');
Route::post('/delete-cart/{id}','ProductController@deleteCart');
