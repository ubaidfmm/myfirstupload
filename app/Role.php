<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    Const ROLE_ADMIN =  10 ;
    Const ROLE_CUSTOMER = 20 ;
}
