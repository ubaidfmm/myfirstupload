<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $with = [
        "roles"
    ];
    protected $appends = [
        "is_admin"
    ];

    protected $visible =[
        'id','name','email','is_admin','created_at','updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
        
    public function getIsAdminAttribute()
    {
        return $this->roles->pluck('name')->contains('admin');
    }
    public function roles(){
        return $this->belongsToMany(Role::class,'role_user');
    }
}
