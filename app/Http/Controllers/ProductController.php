<?php

namespace App\Http\Controllers;


use App\Product;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function product()
    {
        return view('admin.products');
    }


    public function productInsert(Request $request)
    {
        $product = $request ->all();

        if($request->hasFile('image')) {

            $file=$request->file('image');

            $product_image= Storage::putFile('images',$file);

            $data = [
                'product_name'=> $product['product_name'],
                'product_description' => $product['product_description'],
                'product_price' => $product['product_price'],
                'product_image' => $product_image
            ];

        $insertModel  = Product::create($data);
            return back()->with('success', 'Data inserted Successfully');
         

        }

    }

    public function allProduct(){
        $products = \App\Product::all();
        return view('admin.adminProductView', ['data' => $products]);
    }


    public function productEdit(Request $request, $id)
    {
        $product = $request->all();
        $updateModel = Product::find($id);

        return view('admin.productEdit', ['data' => $updateModel]);

    }


    public function productupdated(Request $request, $id){
        $product = $request->all();
        $input = Product::find($id);

        if($input) {
            $request->hasFile('image');
            $file= $request->file('image');

            $product_image= Storage::putFile('images',$file);


            $data = [
                'product_name'=> $product['product_name'],
                'product_description' => $product['product_description'],
                'product_price' => $product['product_price'],
                'product_image' => $product_image
            ];

            $update  = $input->update($data);

            return back()->with('success', 'Data inserted Successfully');
        }
    }

    public function productDelete(Request $request, $id){
//
        $product =  Product::find($id);

        return view('admin.deleteProduct',compact('product'));
    }

    public function deletedproduct($id,Request $request){

        $product = Product::where('id',$id)->delete();
        return redirect('/view-product');


    }

    public function purchaseProduct(){

         $data = Product::all();
         return view('customer.purchaseproduct',['data' => $data ]);
    }

    public function cartProduct(Request $request, $id)
    {

//        $request->session()->flush();//        dd($request->session()->all());
//        dd($request->session()->all());

        $input = $request->all();
        $product = Product::find($id);

        $cart = session('cart');

        // if cart is empty then this the first product

        $cart_data = [
            "id" => $product['id'],
            "product_name" => $product->product_name,
            "quantity" => $input['quantity'],
            "product_price" => $product->product_price,
            "product_image" => $product->product_image


            ];


        Session::push('cart', $cart_data);

        return response()->json(['data' => "Product added successfully"]);



    }
    public function viewCartProduct(){
//        $product =  Product::find($id);

        $cart = session()->get('cart');

       $total_price = 0 ;
       if(!empty($cart)){
           foreach ($cart as $values){
              $total_price += $values['product_price'] * $values['quantity'];

           }
             $total_price ;

                echo "khan";
           return view('customer.viewcart',['total_price' => $total_price, 'cart' => $cart]);
       }


    }
    public function deleteCart($id, Request $request){

        $cart = Session::get('cart');
//        unset($cart[2]);
//        dd($cart);
        unset($cart[$id]);
        session()->put('cart',$cart);
        return response()->json(['data' => "Product deleted successfully"]);
//        if($request->id)
//        {
//
//            $cart = session()->get('cart');
//            dd($cart[0]->['id']);
//            if(isset($cart[$request->id])){
//                unset($request->id);
//                session()->put('cart',$cart);
//            }
//            session()->flash('success', 'Product removed successfully');
//        }
    }
}

