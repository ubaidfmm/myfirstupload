<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<div class="container">

    <table class="table table-striped">

        <tr>
            <th>Product Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>


        </tr>

        @foreach($cart as $key => $row)
            <tr>
                <td>{{$row['product_name']}}</td>
                <td>{{ $row['product_price']}}</td>
                <th><img src="{{asset('public/storage/'.$row['product_image'])}}" height="70px" width="70px" /></th>
                <td><input type="number" class="form-control text-center quantity" value="{{$row['quantity']}}"></td>
                <td> <button type="button" data-id="{{$key}}" class="btn btn-primary delcartbutton">Delete from Cart</button></td>
            </tr>

        @endforeach

    </table>
    <script>
        $(document).ready(function() {
            $('.delcartbutton').on('click', function (e) {
                e.preventDefault();
                var del = $(this).closest("tr").find(".delcartbutton").val();
                $.ajax({
                    url: '{{url("delete-cart")}}'+'/'+$(this).data('id'),
                    method: 'post',
                    data:{_token: '{{ csrf_token() }}'},
                    //    data: $(this).closest(tr).find('delcartbutton').val(),
                    success: function (response) {
                        window.location.reload();
                    }

                })

            })
        })
    </script>
</div>
</body>
</html>