<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<div class="container">

    <table class="table table-striped">

        <tr>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Cart Product</th>

        </tr>
        @foreach($data as $row)
            <tr>
                <td>{{$row->product_name}}</td>
                <td>{{$row->product_description}}</td>
                <td>{{ $row->product_price }}</td>
                <th><img src="{{asset('public/storage/'.$row->product_image)}}" height="70px" width="70px" /></th>
                <td><input type="number" class="form-control text-center quantity" placeholder="Quantity" ></td>
                <td> <button type="button" data-id="{{$row->id}}" class="btn btn-primary jcartbutton">Add To Cart</button></td>
            </tr>

        @endforeach

    </table>

        <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
            <a href="{{ url('view-cart/') }}" class="btn btn-primary btn-block">View all</a>
        </div>

</div>
<script>
    $(document).ready(function(){
        $('.jcartbutton').on('click',function (e) {
            e.preventDefault();
            var quantity = $(this).closest("tr").find(".quantity").val();
            // console.log(quantity);

            $.ajax({
                url:'{{url('cart-product')}}'+'/'+$(this).data('id'),
                method:'post',
                data:{_token: '{{ csrf_token() }}',quantity: quantity },
                success:function (response) {
                    // window.location.reload();
                    // console.log(response);
                }


            });
        })
    });

</script>
</body>
</html>
{{--@php--}}
    {{--$cart = session()->get('cart');--}}
    {{--dd($cart);--}}
{{--@endphp--}}

<style>

    input[type="number" ]{
        width: 100px;
    }
</style>