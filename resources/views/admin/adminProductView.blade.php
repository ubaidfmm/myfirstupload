<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<div class="container">

    <table class="table table-striped">

        <tr>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Image</th>
            <th>Price</th>
            <th>Update Product</th>
            <th>Delete Product</th>
        </tr>
        @foreach($data as $row)
            <tr>
                <td>{{$row->product_name}}</td>
                <td>{{$row->product_description}}</td>
                <th><img src="{{asset('public/storage/'.$row->product_image)}}" height="30px" width="30px" /></th>
                <td>{{$row->product_price}}</td>
                <td> <a href="{{url('product-edit/'.$row->id)}}" /> <button type="button" class="btn btn-primary">Update</button></td>
                <td> <a href="{{url('delete-product/'.$row->id)}}" /> <button type="button" class="btn btn-primary">Delete</button></td>
            </tr>
        @endforeach
    </table>




</div>
</body>
</html>
