<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="post" action="{{url('delete_product_data',['id' => $product->id])}}">

    @method('delete')
    @csrf


    <div class="field">

        <div class="control">
            <p>Are you sure you wish to delete this record</p>
            <button type="submit" class="button is-link">Delete Task</button>

        </div>

    </div>
</form>
</body>
</html>