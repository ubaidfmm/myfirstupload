<html>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href=
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script src=
            "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>

    <script src=
            "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js">
    </script>

    <script src=
            "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
    </script>
</head>
<body>
<div class="container">
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        {{--<form action="{{url('create-task')}}"  method="post">--}}
        <form action="{{url('/insert-product')}}" method="post" enctype="multipart/form-data" >
            @csrf
            <input type="hidden" name="id" value="1">


            <div class="form-group">
                <label for="task">Product Name</label>
                <input type="text" class="form-control"  placeholder="Product Name" name="product_name">
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="task_duratn">Product Description</label>--}}
                {{--<input type="text" class="form-control" placeholder="Product Description" name="task_duration" required>--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="comment">Product Description</label>
                <textarea class="form-control" rows="3" id="comment" name="product_description"></textarea>
            </div>


            <div class="form-group">
                <label for="created_at">Created at</label>
                <input type="date" class="form-control" placeholder="Enter Created date" name="created_at">
            </div>

            <div class="form-group">
                <label for="update_at">Updated at</label>
                <input type="date" class="form-control" placeholder="Enter updated date" name="updated_at">
            </div>

            <div class="form-group">
                <label for="task">Product Price</label>
                <input type="text" class="form-control"  placeholder="Product Price" name="product_price">
            </div>

            <div class="form-group">
                <label for="image">Product Image upload</label>
                </br>
                <input type="file"  name="image">
            </div>

            <button type="submit" class="btn bg-success" id="submitform">
                Submit
            </button>

            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </form>
    </div>
</div>
</body>
</html>